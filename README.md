## Score API

This API simulates scores of people. It is used to calculate a random 
score for a specific person.

### Install the following tools

- jdk 1.8
- maven 3.5

### Getting Started

This is a spring boot application wrapped as maven module. So, after cloning the repository,
please go to the project root folder and execute the following commands:

- Generate the binary distribution

`$ mvn clean package`

- Run the application

```
$ cd target
$ java -jar score-api-0.0.1-SNAPSHOT.jar

```

The above will run the mock api in port [1987](http://localhost:1987)