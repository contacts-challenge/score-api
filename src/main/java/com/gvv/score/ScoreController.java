package com.gvv.score;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@RestController
@RequestMapping("/score-api/v1")
public class ScoreController {

    private Set<ProspectDTO> prospectSet = new HashSet<>();

    public ScoreController(){
        prospectSet.add(new ProspectDTO("123", "German Valencia"));
        prospectSet.add(new ProspectDTO("456", "Ruth Vargas"));
        prospectSet.add(new ProspectDTO("789", "German Contreras"));
        prospectSet.add(new ProspectDTO("012", "Nicolas"));
        prospectSet.add(new ProspectDTO("345", "Camilo"));
        prospectSet.add(new ProspectDTO("678", "Adriana"));
        prospectSet.add(new ProspectDTO("901", "Milciades"));
    }

    @GetMapping("/prospect/{publicId}/score")
    public ResponseEntity<ScoreDTO> getScoreByPublicId(@PathVariable String publicId){
        return prospectSet.stream()
            .filter(prospectDTO -> prospectDTO.getPublicId().equalsIgnoreCase(publicId))
            .findAny()
            .map(prospectDTO -> ResponseEntity.ok().body(new ScoreDTO(prospectDTO.getPublicId(), getScore())))
            .orElseGet(() -> ResponseEntity.noContent().build());
    }

    private static int getScore() {
        Random ran = new Random();
        return ran.nextInt(100) + 1;
    }
}
